@extends('admin.includes.header')

    <img src="{{url('/public/assets/admin/img/placeholders/backgrounds/login_bg.jpg')}}" alt="Login Full Background" class="full-bg animation-pulseSlow">
    <div id="login-container" class="animation-fadeIn">
      <div class="login-title text-center">
        <h1><strong>Book My Cabin</strong><br><br><small>Please <strong>Login</strong></small></h1>
      </div>
      <div class="block push-bit">
      <form action="{{ route('login') }}" method="post" id="admin_login_form" class="form-horizontal form-bordered form-control-borderless">
          
          @csrf

            <div class="form-group">
                <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                    <input id="email" placeholder="Email" type="email" class="form-control input-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                        <input id="password" placeholder="Password" type="password" class="form-control input-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-4">
                    <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">
                        <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
                        <span></span>
                    </label>
                </div>
                <div class="col-xs-8 text-right">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <a href="javascript:void(0)" id="link-reminder-login"><small>Forgot password?</small></a>
                </div>
            </div>
        </form>
        <form action="login_full.php#reminder" method="post" id="form-reminder" class="form-horizontal form-bordered form-control-borderless display-none">
          <div class="form-group">
            <div class="col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                <input type="text" id="reminder-email" name="reminder-email" class="form-control input-lg" placeholder="Email">
              </div>
            </div>
          </div>
          <div class="form-group form-actions">
            <div class="col-xs-12 text-right">
              <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Reset Password</button>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12 text-center">
              <small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
            </div>
          </div>
        </form>
        <form action="login_full.php#register" method="post" id="form-register" class="form-horizontal form-bordered form-control-borderless display-none">
          <div class="form-group">
            <div class="col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                <input type="text" id="register-firstname" name="register-firstname" class="form-control input-lg" placeholder="Full Name">
              </div>
            </div>            
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                <input type="text" id="register-email" name="register-email" class="form-control input-lg" placeholder="Email">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                <input type="password" id="register-password" name="register-password" class="form-control input-lg" placeholder="Password">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                <input type="password" id="register-password-verify" name="register-password-verify" class="form-control input-lg" placeholder="Verify Password">
              </div>
            </div>
          </div>
          <div class="form-group form-actions">
            <div class="col-xs-6">
              <a href="#" data-toggle="modal" class="register-terms">Terms</a>
              <label class="switch switch-primary" data-toggle="tooltip" title="Agree to the terms">
              <input type="checkbox" id="register-terms" name="register-terms">
              <span></span>
              </label>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12 text-center">
              <small>Do you have an account?</small> <a href="javascript:void(0)" id="link-register"><small>Login</small></a>
            </div>
          </div>
        </form>
      </div>
@include('admin.includes.footer')