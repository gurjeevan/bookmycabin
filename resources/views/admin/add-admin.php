<?php include 'includes/header.php'; ?> 
<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="block">
            <div class="block-title">
                <h2><strong>Add Admin</strong></h2>
            </div>
            <form action="#" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Name">Name</label>
                    <div class="col-sm-9">
                        <input type="text" id="Name" name="Name" class="form-control" placeholder="Enter Name..">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Email">Email</label>
                    <div class="col-sm-9">
                        <input type="email" id="Email" name="Email" class="form-control" placeholder="Enter Email..">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Password">Password</label>
                    <div class="col-sm-9">
                        <input type="password" id="Password" name="Password" class="form-control" placeholder="Enter Password..">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Number">Mobile Number</label>
                    <div class="col-sm-9">
                        <input type="text" pattern="[0-9]{10}" maxlength="10" id="Number" name="Number" class="form-control" placeholder="Enter Mobile Number..">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Status">Status</label>
                    <div class="col-sm-9">
                        <label class="radio-inline" for="Active">
                        <input type="radio" id="Active" name="Status"> Active
                        </label>
                        <label class="radio-inline" for="Inactive">
                        <input type="radio" id="Inactive" name="Status"> Inactive
                        </label>				
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-md btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include 'includes/footer.php'; ?>