<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <title>Book My Cabin || Admin Panel</title>
    <meta name="description" content="">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
    <link rel="shortcut icon" href="img/placeholders/avatars/favicon.png">
    <link rel="stylesheet" href={{url('/public/assets/admin/css/bootstrap.min.css')}}>
    <link rel="stylesheet" href={{url('/public/assets/admin/css/plugins.css')}}>
    <link rel="stylesheet" href={{url('/public/assets/admin/css/main.css')}}>
    <link rel="stylesheet" href={{url('/public/assets/admin/css/themes.css')}}>
    <script src={{url('/public/assets/admin/js/vendor/modernizr-respond.min.js')}}></script>
  </head>
  <body>
  @if(Auth::user() != '')
    @section('sidebar-content')
      <div id="page-wrapper">
        <div class="preloader themed-background">
          <h1 class="push-top-bottom text-light text-center"><strong>Book My Cabin</strong></h1>
          <div class="inner">
            <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
            <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
          </div>
        </div>
        <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">

          @include('admin.includes.sidebar')

          <div id="main-container">
            <header class="navbar navbar-default">
              <ul class="nav navbar-nav-custom">
                <li>
                  <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                  <i class="fa fa-bars fa-fw"></i>
                  </a>
                </li>
              </ul>
              <ul class="nav navbar-nav-custom pull-right">
                <li class="dropdown">
                  <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{url('/public/assets/admin/img/placeholders/avatars/user.png')}}" alt="avatar"> <i class="fa fa-angle-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                    <li class="dropdown-header text-center">Account</li>
                    <li class="divider"></li>
                    <li>
                      <a href="javascript:void">
                        <i class="fa fa-sign-out fa-fw pull-right"></i>
                        Logout
                      </a>
                      <a href="#modal-user-settings" data-toggle="modal">
                        <i class="fa fa-cog fa-fw pull-right"></i>
                        Settings
                      </a>
                    </li>
                    <li class="divider"></li>
                  </ul>
                </li>
              </ul>
            </header>
            <div id="page-content">
              <div class="content-header content-header-media inter_page_header">
                <div class="header-section header_bg">
                  <div class="row">
                    <div class="col-md-4 col-lg-6">
                      <h1>Welcome <strong>{{ucfirst(Auth::user()->name)}}</strong></h1>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
    @endsection
@endif