</div>
<footer class="clearfix">
  <div class="pull-left">
    2016 - 2017 &copy; <a href="javascript:void();" target="_blank">dreamhomes.com</a>
  </div>
</footer>
</div>
</div>
</div>
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>
<script src={{url('/public/assets/admin/js/vendor/bootstrap.min.js')}}></script>
<script src={{url('/public/assets/js/form-validator.min.js')}}></script>
<script src={{url('/public/assets/js/form-validation.js')}}></script>
<script src={{url('/public/assets/admin/js/plugins.js')}}></script>
<script src={{url('/public/assets/admin/js/app.js')}}></script>
<script src="//maps.google.com/maps/api/js?sensor=true"></script>
<script src={{url('/public/assets/admin/js/helpers/gmaps.min.js')}}></script>
<script src={{url('/public/assets/admin/js/pages/tablesDatatables.js')}}></script>
<script>$(function(){ TablesDatatables.init(); });</script>
<script src={{url('/public/assets/admin/js/helpers/ckeditor/ckeditor.js')}}></script>
<script src={{url('/public/assets/admin/js/pages/index.js')}}></script>
<script>$(function(){ Index.init(); });</script>
</body>
</html>