@include('admin.includes.header')
@yield('sidebar-content')
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="block">
                <div class="block-title">
                    <h2>
                        <strong>
                            @if($userData != '')
                                {{'Edit User'}}
                            @else
                                {{'Add User'}}
                            @endif
                        </strong>
                    </h2>
                </div>
                <form action="{{url('admin/add_users')}}" id="admin_add_users" method="post" class="form-horizontal form-bordered">
                    @csrf
                    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name" value="{{old('first_name')}}">
                            {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('middle_name') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Middle Name</label>
                        <div class="col-sm-9">
                            <input type="text" id="middle_name" name="middle_name" class="form-control" placeholder="Middle Name" value="{{old('middle_name')}}">
                            {!! $errors->first('middle_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" value="{{old('last_name')}}">
                            {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Email</label>
                        <div class="col-sm-9">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email" value="@if($userData != '') {{$userData->email}}@else {{old('email')}} @endif">
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Password</label>
                        <div class="col-sm-9">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">Gender</label>
                        <div class="col-sm-9">
                            <select name="gender" class="form-control">
                                <option value="">Select Gender...</option>
                                <option value="Female">Female</option>
                                <option value="Male">Male</option>
                            </select>
                            {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('user_type') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label" for="title">UserType</label>
                        <div class="col-sm-9">
                            <select name="user_type" class="form-control">
                                <option value="">Select UserType...</option>
                                <option value="vendor">Vendor</option>
                                <option value="enduser">EndUser</option>
                            </select>
                            {!! $errors->first('user_type', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Status">Status</label>
                        <div class="col-sm-9">
                            <label class="radio-inline" for="Active">
                                <input type="radio" id="Active" name="status" value="1" checked> Active
                            </label>
                            <label class="radio-inline" for="Inactive">
                                <input type="radio" id="Inactive" value="0" name="status"> Inactive
                            </label>				
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@include('admin.includes.footer')