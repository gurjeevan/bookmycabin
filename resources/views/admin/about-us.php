<?php include 'includes/header.php'; ?> 

<div class="row">
<div class="col-sm-10 col-sm-offset-1">

	<!-- Horizontal Form Block -->
	<div class="block">
	
	<!-- Horizontal Form Title -->
	<div class="block-title">
		<h2><strong>About Us</strong></h2>
	</div>
	<!-- END Horizontal Form Title -->

	<!-- Horizontal Form Content -->
	<form action="#" method="post" class="form-horizontal form-bordered">
		<div class="form-group">
			<fieldset>
			<legend>About Us Content</legend>
			<div class="form-group">
				<div class="col-xs-12">
					<textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"></textarea>
				</div>
			</div>
		</fieldset>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="Status">Status</label>
			<div class="col-sm-9">
				<label class="radio-inline" for="Active">
					<input type="radio" id="Active" name="Status"> Active
				</label>
				<label class="radio-inline" for="Inactive">
					<input type="radio" id="Inactive" name="Status"> Inactive
				</label>				
			</div>
		</div>
		<div class="form-group form-actions">
			<div class="col-sm-12 text-center">
				<button type="submit" class="btn btn-md btn-primary">Update</button>
			</div>
		</div>
	</form>
	<!-- END Horizontal Form Content -->
</div>
<!-- END Horizontal Form Block -->

</div>
</div>

<?php include 'includes/footer.php'; ?>