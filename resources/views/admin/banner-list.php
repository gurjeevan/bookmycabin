<?php include 'includes/header.php'; ?> 

<!-- Datatables Content -->
<div class="block full">
   
   <div class="block-title">
      <h2><strong>All Admin List</strong></h2>
   </div>
   
   <div class="table-responsive">
      <table class="table table-vcenter table-condensed table-bordered table_design">
         <div class="sort_search">
		 <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
		 <div class="form-group">
			<select id="cus_sort_by" name="cus_sort_by" class="form-control">
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option value="All">All</option>
			</select>
		 </div>
		 </div>
		 <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 pull-right">
		 <div class="input-group">
			<input type="text" id="search_keyword" name="search_keyword" class="form-control" placeholder="Search">
			<span class="input-group-btn">
				<button type="button" class="btn btn-primary">Search</button>
			</span>
		 </div><br>
		 </div>
		 </div>
		 <thead>
            <tr>
               <th class="text-center">S.No</th>               
               <th class="text-center">Image</th>
               <th class="text-center">Title</th>
               <th class="text-center">Status</th>
               <th class="text-center">Actions</th>
            </tr>
         </thead>
		 
         <tbody>
            <tr>
               <td class="text-center">1</td>
               <td class="text-center"><img src="img/placeholders/photos/photo1.jpg" width="50" height="50"></td>
               <td class="text-center">Title</td>
               <td class="text-center"><label class="label label-success">Active</label></td>
			   <td class="text-center">
				<div class="btn-group">
					<a href="update-banner.php" data-toggle="tooltip" title="Edit" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
				</div>
			    </td>	
            </tr>
			<tr>
               <td class="text-center">2</td>
               <td class="text-center"><img src="img/placeholders/photos/photo1.jpg" width="50" height="50"></td>
			   <td class="text-center">Title</td>
               <td class="text-center"><label class="label label-danger">Inactive</label></td>
			   <td class="text-center">
				<div class="btn-group">
					<a href="update-banner.php" data-toggle="tooltip" title="Edit" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
				</div>
			    </td>	
            </tr>
            
         </tbody>
		 
      </table>
   </div>
</div>
<!-- END Datatables Content -->

<?php include 'includes/footer.php'; ?>