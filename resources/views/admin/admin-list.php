<?php include 'includes/header.php'; ?> 

<!-- Datatables Content -->
<div class="block full">
   
   <div class="block-title">
      <h2><strong>All Admin List</strong></h2>
   </div>
   
   <div class="table-responsive">
      <table class="table table-vcenter table-condensed table-bordered table_design">         
		<thead>
            <tr>
               <th class="text-center">S.No</th>               
               <th class="text-center">Name</th>
               <th class="text-center">Email</th>
               <th class="text-center">Password</th>
               <th class="text-center">Mobile Number</th>
               <th class="text-center">Status</th>
               <th class="text-center">Actions</th>
            </tr>
        </thead>
		 
        <tbody>
            <tr>
               <td class="text-center">1</td>
               <td class="text-center">M. Abid</td>
               <td class="text-center">abid@gmail.com</td>
               <td class="text-center">123456</td>
               <td class="text-center">+92 301 2345678</td>
               <td class="text-center"><label class="label label-success">Active</label></td>
			   <td class="text-center">
				<div class="btn-group">
					<a href="update-admin.php" data-toggle="tooltip" title="Edit" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
				</div>
			    </td>	
            </tr>
			<tr>
               <td class="text-center">2</td>
               <td class="text-center">M. Abid</td>
               <td class="text-center">abid@gmail.com</td>
               <td class="text-center">123456</td>
               <td class="text-center">+92 301 2345678</td>
               <td class="text-center"><label class="label label-danger">Inactive</label></td>
			   <td class="text-center">
				<div class="btn-group">
					<a href="update-admin.php" data-toggle="tooltip" title="Edit" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i></a>
					<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
				</div>
			    </td>	
            </tr>            
        </tbody>		 
      </table>
   </div>
</div>
<!-- END Datatables Content -->

<?php include 'includes/footer.php'; ?>