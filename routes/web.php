<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*===============Admin routing============*/

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/logout', 'AdminController@logout')->name('logout');

Route::get('/admin/add_users', 'AdminController@addUsers')->name('add_users');

Route::post('/admin/add_users', 'AdminController@addUsers')->name('add_users');

Route::get('/admin/add_users/{id}', 'AdminController@addUsers');

Route::get('/admin/manage_users', 'AdminController@manageUsers')->name('manage_users');



