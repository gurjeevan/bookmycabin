<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use Hash;
use App\User;
use App\UserMeta;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index');
    }

    public function logout()
    {
        if(Auth::user())
        {
            Auth::logout();
        }
       
        redirect('/admin');
    }

    /*=====================Add users by admin===============*/

    public function addUsers(Request $request, $id)
    {
        $userData = '';
        if($id != '')
        {
            $userData = User::with('userMeta')->where('uniqueId',$id)->first();
            dd($userData);
        }
        if($request->all())
        {
            
            $validator = $request->validate([
                'email' => 'required|unique:users',
                'first_name' => 'required',
                'middle_name' => 'required',
                'last_name' => 'required',
                'password' => 'required|min:6',
                'gender' => 'required',
                'user_type' => 'required',
            ]);
            $randomStr = rand(); 
            $user = new User();
            $user->name = $request->first_name;
            $user->email = $request->email;
            $user->uniqueId = md5($randomStr);
            $user->password = Hash::make($request->password);
            $user->status = $request->status;
            $user->user_type = $request->user_type;
            $user->email_verification = 'no';
            $user->save();
            
            $user_meta = new UserMeta();
            $user_meta->user_unique_id = md5($randomStr);
            $user_meta->first_name = $request->first_name;
            //$user_meta->middle_name = 'ds';
            $user_meta->last_name = $request->last_name;
            $user_meta->gender = $request->gender;
            $user_meta->save();

            return redirect('admin/manage_users');
        }
        return view('admin.add_users',['userData'=>$userData]);
    }

    /*================Show user listing==============*/

    public function manageUsers()
    {
        $users = User::where('user_type','!=','admin')->get();
        return view('admin.manage_users',['users'=>$users]);
    }
}