jQuery(document).ready(function(){
    jQuery("#admin_login_form").validate({
        rules: {
          password: "required",
          email: {
            required: true,
            email: true
          }
        },
        messages: {
          password: "Password required",
          email: {
            required: "Email required",
            email: "Your email address must be in the format of name@domain.com"
          }
        }
    });

    // jQuery("#admin_add_users").validate({
    //   rules: {
    //     first_name: "required",
    //     middle_name: "required",
    //     last_name: "required",
    //     password: "required",
    //     gender: "required",
    //     user_type: "required",
    //     email: {
    //       required: true,
    //       email: true
    //     }
    //   },
    //   messages: {
    //     first_name: "First name required",
    //     middle_name: "Middle name required",
    //     last_name: "Last name required",
    //     password: "Password required",
    //     gender: "Gender required",
    //     user_type: "UserType required",
    //     email: {
    //       required: "Email required",
    //       email: "Your email address must be in the format of name@domain.com"
    //     }
    //   }
    // });
});